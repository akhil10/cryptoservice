package com.data.cryptoservice.servlets;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TwitterServlet extends HttpServlet {
    private static Logger log = LoggerFactory.getLogger(TwitterServlet.class);

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        log.info("Accessing TwitterServlet, request: {}", req.getMethod());
        resp.setStatus(HttpStatus.OK_200);
        resp.getWriter().println("Hello World!");
    }

    protected  void doPost(HttpServletRequest req, HttpServletResponse resp) {
        log.info("Accessing TwitterServlet, request: {}", req.getMethod());
    }

}
