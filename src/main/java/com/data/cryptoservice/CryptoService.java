package com.data.cryptoservice;

import com.data.cryptoservice.services.Twitter.TwitterQueries;
import com.data.cryptoservice.servlets.TwitterServlet;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;


public class CryptoService {

    private static Logger log = LoggerFactory.getLogger(CryptoService.class);
    //static OAuthConsumer consumer;


    private static int getPort() {
        int port = 8085;
        //TODO: set environment variables
        return port;
    }

    public static void main(String[] args) {
        int port = getPort();
        //BasicConfigurator.configure();
        try {
            log.info("Starting server on port {}", port);
            Server server = new Server(port);
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(port);
            server.setConnectors(new Connector[]{connector});

            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);


            ServletHolder holder = new ServletHolder("Hello World!", new TwitterServlet());
            context.addServlet(holder, "/*");

            log.info("Calling Twitter handler");
            twitterHandler();
            server.start();
            server.join();

        } catch (Exception e) {
            log.error("Unable to open server port {}, error: {}", port, e.getMessage());
        }

    }

    private static void twitterHandler() throws SQLException {
        TwitterQueries twitterQueries = new TwitterQueries();
        twitterQueries.getUserTimeline("Akhil73276450");
        String keyword = "Ethereum";
        twitterQueries.queryTweetsKeyword(keyword);
        twitterQueries.printDatabaseEntries();
    }
}
