package com.data.cryptoservice.services.Twitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class TwitterConnection {
    private static Logger log = LoggerFactory.getLogger(TwitterConnection.class);

    /* You should have already negotiated the access token and access token secret using some other way */
    String consumerKey = "KFXCcKYZ4GE7kSlTLGw0zf5l5";
    String consumerSecret = "x3CX67zbEZNEkXuu0TkQNmiPodGNTB7UTujmyVmobMgPQnBhMg";
    String accessToken = "924067685608054785-jKFFyFl8VYzNv49gPqkmO8XxF9s1OSK";
    String accessTokenSecret = "WNYgnl2pDaH3ySob82Tp8x4dODT7i4MTuj4RGWphTuGKM";
    private Twitter twitter;

    public TwitterConnection() {

    }

    public Twitter getTwitterConnection() {
        if (twitter == null) {
            log.info("Creating new TwitterFactory instance");
            twitter = new TwitterFactory().getInstance();
            twitter.setOAuthConsumer(consumerKey, consumerSecret);
            twitter.setOAuthAccessToken(new AccessToken(accessToken, accessTokenSecret));
        }
        return twitter;
    }

}
