package com.data.cryptoservice.services.Twitter;

import com.data.cryptoservice.services.Postgres.PostgresClientConnection;
import com.data.cryptoservice.services.Postgres.PostgresDatabaseQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public class TwitterQueries {
    private static Logger log = LoggerFactory.getLogger(TwitterQueries.class);
    private Twitter twitter;
    private PostgresClientConnection postgresClientConnection;
    private PostgresDatabaseQuery postgresDatabaseQuery;
    private Connection databaseCon;

    public TwitterQueries() {
        TwitterConnection tcon = new TwitterConnection();
        twitter = tcon.getTwitterConnection();
        postgresClientConnection = new PostgresClientConnection();
        postgresDatabaseQuery = new PostgresDatabaseQuery();
    }

    /**
     * Get the postgres Postgres connection.
     */
    private Connection getDatabaseCon() {
        databaseCon = postgresClientConnection.getConnection();
        return databaseCon;
    }

    /**
     * Get the first five tweets for a given user timeline
     * @param user user
     */
    public void getUserTimeline(String user) {
        try {
            log.info("Getting @{}'s timeline", user);
            List<Status> statusList = twitter.getUserTimeline(new Paging(1,5));
            for (Status status : statusList) {
                log.info("@{} - {}", status.getUser().getScreenName(), status.getText());
            }
        } catch (TwitterException te) {
            // Check if key/secret has been set correctly
            if (!twitter.getAuthorization().isEnabled()) {
                log.error("OAuth consumer key/secret is not set.");
                System.exit(-1);
            }

            log.error("Failed to get user {}'s timeline, error: {}", user, te.getMessage());
            te.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Query twitter API for all tweets matching given keyword.
     * TODO: Extend the function to query OR of multiple keywords
     * TODO: https://stackoverflow.com/questions/17344921/twitter4j-multiple-queries-twitter-api-1-1
     * @param keyword Keyword to search
     */
    public void queryTweetsKeyword(String keyword) throws SQLException {
        databaseCon = getDatabaseCon();

        try {
            Query query = new Query(keyword);
            //query.setCount(5);
            QueryResult result;

            int count = 0;
            int inserted = 0;

            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                    String user = tweet.getUser().getScreenName();
                    String message = tweet.getText();
                    Date created = tweet.getCreatedAt();
                    log.info("@{}: {} - {}", user, created, message);

                    //replace the char "\'" with empty space, so that message is stored correctly in Postgres.
                    message = message.replaceAll("\'", "");
                    Timestamp timestamp = new Timestamp(created.getTime());
                    inserted += postgresDatabaseQuery.insert(databaseCon, user, timestamp, message);
                    count++;
                }
            } while ((query = result.nextQuery()) != null);// || count < 5);
            log.info("Total tweets received: {}, inserted to Postgres: {}", count, inserted);
            log.info("Closing Postgres connection");
            databaseCon.close();
        } catch(TwitterException te) {
            log.error("Unable to query twitter for keyword {}, error: {}", keyword, te.getMessage());
            databaseCon.close();
            System.exit(-1);
        }
    }

    public void printDatabaseEntries() throws SQLException {
        databaseCon = getDatabaseCon();
        postgresDatabaseQuery.select(databaseCon);
        log.info("Closing postgres Postgres connection");
        databaseCon.close();
    }
}
