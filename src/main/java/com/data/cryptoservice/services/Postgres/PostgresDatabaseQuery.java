package com.data.cryptoservice.services.Postgres;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class PostgresDatabaseQuery {
    private static Logger log = LoggerFactory.getLogger(PostgresDatabaseQuery.class);

    /**
     * Insert an entry to the Postgres
     * @param connection Postgres connection
     * @param user user_id
     * @param timestamp Tweet creation time
     * @param tweet tweet message
     * @return count of entries inserted
     */
    public int insert(Connection connection, String user, Timestamp timestamp, String tweet) {
        String sql = "INSERT INTO \"Tweets\" (user_id, created, message) VALUES (?, ?, ?);";
        int count = 0;
        log.info("Inserting record for user {} to postgres", user);
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sql);
            connection.setAutoCommit(false);
            statement.setString(1, user);
            statement.setTimestamp(2, timestamp);
            statement.setString(3, tweet);

            count += statement.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            log.error("Unable to insert record into the Postgres, error: {}", e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

                //can't be closed here, closed after inserting all tweets from current run in calling fn
                if (connection != null) {
                    //connection.close();
                }
            } catch (Exception e) {
                log.error("Unable to close PreparedStatement/connection, error: {}", e.getMessage());
            }
        }
        return count;
    }

    public void select(Connection connection) throws SQLException {
        String sql = "SELECT user_id, created, message FROM \"Tweets\"";
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(sql);

            // execute select statement
            ResultSet resultSet = statement.executeQuery();
            int count = 0;

            while (resultSet.next()) {
                String user_id = resultSet.getString("user_id");
                Timestamp timestamp = resultSet.getTimestamp("created");
                String message = resultSet.getString("message");
                count++;

                log.info("Retrieved from Postgres, user: {}, time: {}, tweet: {}", user_id, timestamp, message);
            }
            log.info("Received {} entries from Postgres", count);
        } catch (Exception e) {
            log.error("Unable to retrieve entries from Postgres, error: {}", e.getMessage());
        }
    }
}
