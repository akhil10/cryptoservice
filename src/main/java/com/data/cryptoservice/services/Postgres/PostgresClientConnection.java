package com.data.cryptoservice.services.Postgres;
/*
 * JDBC driver connection from Java to Postgresql
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Properties;

public class PostgresClientConnection {

    private static Logger log = LoggerFactory.getLogger(PostgresClientConnection.class);
    private Connection connection = null;
    private Properties properties = new Properties();

    /**
     * Get the postgres Postgres connection to Postgres Akhil
     * @return connection
     */
    public Connection getConnection() {
        String url = "jdbc:postgresql://localhost:5432/Akhil";
        properties.setProperty("user", "postgres");
        properties.setProperty("password", "postgres");
        try {
            log.info("Opening connection to postgres..");
            connection = DriverManager.getConnection(url, properties);
            log.info("Opened connection to postgres successfully.");
        } catch (Exception e) {
            log.error("Unable to open connection to postgres, err: {}", e.getMessage());
        }
        return connection;
    }

}
