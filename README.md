# CryptoService

##Technologies Used
1. Postgres - Database
2. PgAdmin - Client for Postgres
3. Postico - Client for Postgres
4. Postman - API Development Environment (ADE)
5. Gradle - Build Tool
6. CI - Continuous Integration
7. Intellij - IDE
8. Java Servlets - Http Get and Post
9. Logger, LoggerFactory, logger properties
10. GIT repo

##TODO
1. Learn about Docker, Container
2. Jenkins
3. REST API Java
4. Java Script, CSS, HTML
5. Angular



